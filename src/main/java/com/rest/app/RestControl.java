package com.rest.app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rest.DB.*;
import com.rest.exceptions.*;

@RestController
public class RestControl {

    @RequestMapping(value = "/playerinfo")
    public Player playerInfo(
        @RequestParam(value="id") long id) {
        PlayerDB db = new PlayerDB();
        Player player;
        try {
            player = db.getPlayerWithId(id);
        } catch (PlayerNotFoundException e) {
            player = new Player(-1, "not found");
        }
        return player;
    }
    
    @RequestMapping(value = "/wordlist")
    public WordDictionary wordList(
        @RequestParam(value="lang") String lang) {
        return new WordDictionary(lang.toUpperCase());
    }
}


package com.rest.DB;

public class Player {
	private final long id;
    private final String alias;

    public Player(long id, String alias) {
        this.id = id;
        this.alias = alias;
    }

    public long getId() {
        return id;
    }
    public String getAlias() {
        return alias;
    }
}

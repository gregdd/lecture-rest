package com.rest.DB;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class WordDictionary {
	List<String> wordList;

	public WordDictionary(String lang){
		wordList = new ArrayList<>();
		populateList(lang);
	}
	
	public List<String> getWordList() {
		return wordList;
	}

	public void setWordList(List<String> wordList) {
		this.wordList = wordList;
	}

	private void populateList(String lang){
		switch(lang){
		case "FRA":
			wordList = Arrays.asList("pomme", "orange", "voiture", "ecole", "classe", "table");
			break;
		case "ENG":
			wordList = Arrays.asList("student", "desk", "apple", "school", "class", "orange");
			break;
		default:
			wordList.add("LANG NOT FOND");
		
		}
	}
	
}

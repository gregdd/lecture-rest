package com.rest.DB;

import com.rest.exceptions.PlayerNotFoundException;

public class PlayerDB {
	public Player getPlayerWithId(long playerId) throws PlayerNotFoundException {
		// Mockup-up code
		if (3 == playerId) {
			return new Player(3, "nicolas");
		} else {
			throw new PlayerNotFoundException("no match for ID" + playerId);
		}
	}
}
